# -*- coding: utf-8 -*-
import json
import re
import requests
import urllib.request

from bs4 import BeautifulSoup
from flask import Flask, request, make_response
from slack import WebClient
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slack.web.classes.interactions import MessageInteractiveEvent
from slackeventsapi import SlackEventAdapter
from urllib import parse

SLACK_TOKEN = "xoxb-682997044292-691779420102-bGlTFGq3TjPopjMbqvBDZOcs"
SLACK_SIGNING_SECRET = "b5156e1a1c7d7833b75f9f146c770f3a"
prev_client_msg_id = {}
current_num = 0
app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)
#category를 받아오는 페이지
def get_tag_list():
    tag_list = []
    url = "https://www.acmicpc.net/problem/tags"
    request = urllib.request.Request(url,headers={'User-Agent': 'Mozilla/5.0'})
    source_code = urllib.request.urlopen(request).read()
    soup = BeautifulSoup(source_code, "html.parser")
    rows = soup.find("table").find("tbody").find_all("tr")
    f_head_section = SectionBlock(
        text="*BAEKJOON ONLINE JUDGE")
    f_head_images_section = ImageBlock(
        image_url="https://d2gd6pc034wcta.cloudfront.net/images/logo@2x.png",
        alt_text = "이미지를 불러올 수 없습니다."
    )    
    text = ""
    count = 1
    for row in rows:
        if count < 36:
                td = row.find_all("td")
                text = text + (td[0].get_text() + '\n')
                count = count + 1
    tag_list.append(text)
    link_section = SectionBlock(fields=tag_list)
    f_tail_section = SectionBlock(
        text= "\n"+"\n"+"> *출처* : BAEKJOON ONLIINE JUDGE"
        +"\n"+"> *입력방법:* "+" `@Baekjoon` 카테고리를 입력해주세요! ex) '@Baekjoon 다이나믹 프로그래밍'" 

    )
    return [f_head_section, f_head_images_section, DividerBlock(), link_section, DividerBlock(), f_tail_section]

def get_question_list(tag, difficulty):
    DIVIDER = {"type": "divider"}
    MAX_COUNT = 10 + current_num # 문제 출력 개수
    url = "https://www.acmicpc.net/problem/tag/" + parse.quote(tag)
    print(url)
    request = urllib.request.Request(url,headers={'User-Agent': 'Mozilla/5.0'})
    source_code = urllib.request.urlopen(request).read()
    soup = BeautifulSoup(source_code, "html.parser")
    rows = soup.find("table").find("tbody").find_all("tr")
    question_list = []
    sorted_list = []
    s_list = []
    message_block = []
    
    s_head_section = SectionBlock(
        text="*BAEK JOON TEST_*"+'\n'+
        "*\"" + tag + "\"의 문제입니다.*"
    )
    message_block.append(s_head_section)
    s_head_images_section = ImageBlock(
        image_url="https://d2gd6pc034wcta.cloudfront.net/images/logo@2x.png",
        alt_text = "이미지를 불러올 수 없습니다.." )
    message_block.append(s_head_images_section)
    message_block.append(DividerBlock())
    for row in rows:
        td = row.find_all("td")
        success_rate = td[5].get_text().split('.')[0]
        if difficulty == "hard" and int(success_rate) <= 30:
                question_list.append((td[0].get_text(), td[1].get_text(), td[5].get_text()))
        elif difficulty == "medium" and int(success_rate) > 30 and int(success_rate) < 60:
                question_list.append((td[0].get_text(), td[1].get_text(), td[5].get_text()))
        elif difficulty == "easy" and int(success_rate) >= 60:
                question_list.append((td[0].get_text(), td[1].get_text(), td[5].get_text()))
        sort_by_rate = lambda rate: float(rate[2].replace('%',''))
        sorted_list = sorted(question_list, key=sort_by_rate, reverse=True)

        if len(question_list) > MAX_COUNT:
            sorted_list = sorted_list[current_num:MAX_COUNT]
        else:
            sorted_list = sorted_list[current_num:len(question_list)]

    for i in sorted_list:
        text = i[0] + ' - ' + i[1] + ' - ' + i[2]
        question_section1 = ContextBlock(elements = [{
                        "type": "mrkdwn",
                        "text": "*<" + "https://www.acmicpc.net/problem/" + i[0] + "|" + text+ ">*"
                }])
        message_block.append(question_section1)
        message_block.append(DividerBlock())

    button_action = ActionsBlock(
        block_id="next_question",
        elements=[
            ButtonElement(
                text="다음 10문제➡️", 
                action_id="next_10", value=tag + ',' + difficulty
            )
        ]
    )
    message_block.append(button_action)
    return message_block

def select_difficulty(tag):
    head_section = SectionBlock(
        text="> `EASY` `MIDIUM` `HARD` 버튼을 클릭하여 난이도별 문제를 확인하세요!"
    )
    button_action = ActionsBlock(
        block_id="difficulty",
        elements=[
            ButtonElement(
                text="👶🏻EASY", style="primary",
                action_id="easy", value="easy," + tag
            ),
            ButtonElement(
                text="👦🏻MEDIUM",
                action_id="medium", value="medium," + tag
            ),
            ButtonElement(
                text="👨🏻HARD", style="danger",
                action_id="hard", value="hard," + tag
            )
        ]
    )
    tail_section = SectionBlock(
        text= "\n"+"\n"+">출처: `BAEKJOON ONLIINE JUDGE`" 
    )
    return [
            head_section, 
            DividerBlock(),
            button_action, 
            DividerBlock(), 
            tail_section
        ]

@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    message_blocks = []

    client_msg_id = event_data["event"]["client_msg_id"]
    if client_msg_id not in prev_client_msg_id:
        prev_client_msg_id[client_msg_id] = 1
    else:
        resp = make_response('no-retry', 201)
        resp.headers['X-Slack-No-Retry'] = 1
        return resp

    if len(text) == 12:
        print("Notified!!!!!!!")
        message_blocks = get_tag_list()
    elif text.find(" "):
        print("CATEGORY SELECTED!!!!!!!")
        text = text[13:len(text)]
        keyword = text.replace(" ", "_")
        print(keyword)
        message_blocks = select_difficulty(keyword)

    slack_web_client.chat_postMessage(
        channel = channel,
        blocks = extract_json(message_blocks)
    )

@app.route("/click", methods=["GET", "POST"])
def on_button_click():
    payload = request.values["payload"]
    click_event = MessageInteractiveEvent(json.loads(payload))

    button_id = click_event.block_id

    if button_id == "difficulty":
        keyword = click_event.value.split(',')[1]
        difficulty = click_event.value.split(',')[0]
        keyword = keyword.replace("_", " ")
        message_blocks = get_question_list(keyword, difficulty)
    elif button_id == "next_question":
        keyword = click_event.value.split(',')[0]
        difficulty = click_event.value.split(',')[1]
        global current_num
        current_num = current_num + 10
        message_blocks = get_question_list(keyword, difficulty)
    # 메시지를 채널에 올립니다
    slack_web_client.chat_postMessage(
        channel=click_event.channel.id,
        blocks=extract_json(message_blocks)
        
    )
    return "OK", 200

@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"

if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)